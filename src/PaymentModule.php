<?php
/*
 *	Copyright 2015 RhubarbPHP
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
namespace Rhubarb\Scaffolds\Payment;

use Rhubarb\Crown\Module;
use Rhubarb\Scaffolds\Payment\Model\PaymentSchema;
use Rhubarb\Scaffolds\Payment\Providers\PaymentProvider;
use Rhubarb\Stem\Schema\SolutionSchema;

/**
 * Class PaymentModule
 * @package Rhubarb\Scaffolds\Payment
 */
class PaymentModule extends Module
{

    public function __construct( $paymentProviderToUse = null )
    {
        PaymentProvider::setDefaultPaymentProviderClassName( $paymentProviderToUse );
        parent::__construct();
    }

    public function initialise()
    {
        SolutionSchema::registerSchema( "Payment", PaymentSchema::class );
    }
}