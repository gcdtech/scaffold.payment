<?php
/*
 *	Copyright 2015 RhubarbPHP
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
namespace Rhubarb\Scaffolds\Payment\Providers\SagePay;

use Rhubarb\Scaffolds\Payment\Model\Payment;
use Rhubarb\Scaffolds\Payment\Model\PaymentAttempt;
use Rhubarb\Scaffolds\Payment\Providers\PaymentProvider;
use Rhubarb\Scaffolds\Payment\Providers\PaymentProviderException;
use Rhubarb\Stem\Exceptions\RecordNotFoundException;

/**
 * Class SagePay
 * @package Rhubarb\Scaffolds\Payment\Providers\SagePay
 */
class SagePay extends PaymentProvider
{
    /** @var string orderID */
    public $orderID;

    /** @var  $sagePayProtocol */
    public $sagePayProtocol;

    /** @var  float amount */
    public $amount;

    /** @var string currency */
    public $currency;

    /** @var  string sagePayUrl */
    public $sagePayUrl;

    /** @var  string sagePayMode */
    public $sagePayMode;

    /** @var  string transaction type */
    public $transactionType;

    /** @var  string vendorName */
    public $vendorName;

    /** @var  string vendorEmail */
    public $vendorEmail;

    /** @var  int paymentID */
    public $paymentID;

    /** @var  string description */
    public $description;

    /** @var  string successUrl */
    public $successUrl;

    /** @var  string failureUrl */
    public $failureUrl;

    /** @var  string sageSendEmail */
    public $sageSendEmail;

    /** @var  string giftAid */
    public $giftAid;

    /** @var  string payPalAgreement */
    public $payPalAgreement;

    /** @var  string applyAvsCv2 */
    public $applyAvsCv2;

    /** @var  string apply3DSecure */
    public $apply3DSecure;

    /** @var string encryptionType */
    public $encryptionType;

    /** @var string encryptionPassword */
    public $encryptionPassword;

    /** @var string SAGE_PAY_PROTOCOL_VERSION_3 */
    const SAGE_PAY_PROTOCOL_VERSION_3 = "3.0";

    /** @var string SAGE_PAY_LIVE_PURCHASE_URL */
    const SAGE_PAY_LIVE_PURCHASE_URL = "https://live.sagepay.com/gateway/service/vspform-register.vsp";

    /** @var  string SAGE_PAY_TEST_PURCHASE_URL */
    const SAGE_PAY_TEST_PURCHASE_URL = "https://test.sagepay.com/gateway/service/vspform-register.vsp";

    /** @var string SAGE_PAY_SIM_PURCHASE_URL */
    const SAGE_PAY_SIM_PURCHASE_URL = "https://test.sagepay.com/Simulator/VSPFormGateway.asp";

    /** @var string ENCRYPTION_TYPE_AES  */
    const ENCRYPTION_TYPE_AES = "AES";

    /** @var string ENCRYPTION_TYPE_XOR */
    const ENCRYPTION_TYPE_XOR = "XOR";

    /** @var string SAGE_PAY_MODE_LIVE */
    const SAGE_PAY_MODE_LIVE = "LIVE";

    /** @var string SAGE_PAY_MODE_TEST  */
    const SAGE_PAY_MODE_TEST = "TEST";

    /** @var string SAGE_PAY_MODE_SIMULATOR */
    const SAGE_PAY_MODE_SIMULATOR = "SIMULATOR";

    /** @var string CURRENCY_GBP */
    const CURRENCY_GBP = "GBP";

    /** @var string CURRENCY_EURO */
    const CURRENCY_EURO = "EUR";

    /** @var string SAGE_PAY_GIFT_AID_DISABLED */
    const SAGE_PAY_GIFT_AID_DISABLED = 0;

    /** @var string SAGE_PAY_GIFT_AID_ENABLED */
    const SAGE_PAY_GIFT_AID_ENABLED = 1;

    /** @var string SAGE_PAY_PAL_ENABLED */
    const SAGE_PAY_PAY_PAL_ENABLED = 1;

    /** @var string SAGE_PAY_PAY_PAL_DISABLED */
    const SAGE_PAY_PAY_PAL_DISABLED = 0;

    /** @var string SAGE_PAY_TRANSACTION_TYPE_PAYMENT */
    const SAGE_PAY_TRANSACTION_TYPE_PAYMENT = "PAYMENT";

    /** @var string SAGE_PAY_TRANSACTION_TYPE_DEFERRED */
    const SAGE_PAY_TRANSACTION_TYPE_DEFERRED = "DEFERRED";

    /** @var string SAGE_PAY_TRANSACTION_TYPE_AUTHENTICATE */
    const SAGE_PAY_TRANSACTION_TYPE_AUTHENTICATE = "AUTHENTICATE";

    /** @var string SAGE_PAY_APPLY_AVS_CSV2_DEFAULT - If AVS/CV2 enabled then check them. If rules apply, use rules. (default */
    const SAGE_PAY_APPLY_AVS_CSV2_DEFAULT = 0;

    /** @var string SAGE_PAY_APPLY_AVS_CSV2_FORCE_CHECKS - Force AVS/CV2 checks even if not enabled for the account. If rules apply, use rules. */
    const SAGE_PAY_APPLY_AVS_CSV2_FORCE_CHECKS = 1;

    /** @var string SAGE_PAY_APPLY_AVS_CSV2_NO_FORCE_CHECKS - orce NO AVS/CV2 checks even if enabled on account. */
    const SAGE_PAY_APPLY_AVS_CSV2_NO_FORCE_CHECKS = 2;

    /** @var string SAGE_PAY_APPLY_AVS_CSV2_FORCE_CHECKS_DONT_APPLY_RULES - Force AVS/CV2 checks even if not enabled for the account but DON'T apply any rules. */
    const SAGE_PAY_APPLY_AVS_CSV2_FORCE_CHECKS_DONT_APPLY_RULES = 3;

    /** @var string SAGE_PAY_APPLY_3D_SECURE_DEFAULT - If 3D-Secure checks are possible and rules allow, perform the checks and apply the authorisation rules. (default) */
    const SAGE_PAY_APPLY_3D_SECURE_DEFAULT = 0;

    /** @var string SAGE_PAY_APPLY_3D_SECURE_FORCE - Force 3D-Secure checks for this transaction if possible and apply rules for authorisation. */
    const SAGE_PAY_APPLY_3D_SECURE_FORCE = 1;

    /** @var string SAGE_PAY_APPLY_3D_SECURE_NO_CHECKS_AUTH - Do not perform 3D-Secure checks for this transaction and always authorise. */
    const SAGE_PAY_APPLY_3D_SECURE_NO_CHECKS_AUTH = 2;

    /** @var string  SAGE_PAY_APPLY_3D_SECURE_FORCE_CHECKS_IF_POSS_AUTH - Force 3D-Secure checks for this transaction if possible but ALWAYS obtain an auth code, irrespective of rule base. */
    const SAGE_PAY_APPLY_3D_SECURE_FORCE_CHECKS_IF_POSS_AUTH = 3;

    /** @var  string CLEAN_INPUT_FILTER_TEXT */
    const CLEAN_INPUT_FILTER_TEXT = '[:alnum:] \-\.\\\\,\'/{}@():?_ & £$=%~*+"';

    /** @var string CLEAN_INPUT_FILTER_NUMERIC */
    const CLEAN_INPUT_FILTER_NUMERIC = '0-9 \.,';

    /** @var string CLEAN_INPUT_FILTER_ALPHABETIC */
    const CLEAN_INPUT_FILTER_ALPHABETIC = 'A-Za-z ';

    /** @var string CLEAN_INPUT_FILTER_ALPHABETIC_AND_ACCENTED */
    const CLEAN_INPUT_FILTER_ALPHABETIC_AND_ACCENTED = '[:alpha:] ';

    /** @var string  CLEAN_INPUT_FILTER_ALPHABETIC_AND_ACCENTED */
    const CLEAN_INPUT_FILTER_ALPHANUMERIC = 'A-Za-z0-9 ';

    /**  @var string CLEAN_INPUT_FILTER_ALPHANUMERIC_AND_ACCENTED */
    const CLEAN_INPUT_FILTER_ALPHANUMERIC_AND_ACCENTED = '[:alnum:] ';

    /** @var  string customerName */
    public $customerName;

    /** @var  string customerEmail */
    public $customerEmail;

    /** @var  string billingSurname */
    public $billingSurname;

    /** @var  string billingFirstNames */
    public $billingFirstNames;

    /** @var  string billingAddress1 */
    public $billingAddress1;

    /** @var  string billingAddress2 */
    public $billingAddress2;

    /** @var  string billingCity */
    public $billingCity;

    /** @var  string billingPostCode */
    public $billingPostCode;

    /** @var  string billingCountry*/
    public $billingCountry;

    /** @var  string billingState */
    public $billingState;

    /** @var  string billingPhone */
    public $billingPhone;

    /** @var bool deliveryIsSame */
    public $deliveryIsSame = false;

    /** @var string deliverySurname */
    public $deliverySurname;

    /** @var string deliveryFirstNames */
    public $deliveryFirstNames;

    /** @var  string deliveryAddress1 */
    public $deliveryAddress1;

    /** @var string deliveryAddress2 */
    public $deliveryAddress2;

    /** @var string deliveryCity */
    public $deliveryCity;

    /** @var string deliveryPostCode */
    public $deliveryPostCode;

    /** @var string  deliveryCountry */
    public $deliveryCountry;

    /** @var string deliveryState  */
    public $deliveryState;

    /** @var  string  deliveryPhone */
    public $deliveryPhone;

    /** @var array List Of orders */
    public $orderItems = [];


    public function __construct()
    {

    }

    /**
     * @return string
     * @throws PaymentProviderException
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     */
    public function getHtml()
    {
        $payment = $this->getPayment();

        $values = [
            "VendorTxCode" => $payment->TransactionRef,
            "Amount" => $this->amount,
            "Currency" => $this->getCurrency(),
            "Description" => $this->description,
            "SuccessUrl" => $this->successUrl,
            "FailureUrl" => $this->failureUrl,
            "SendEmail" => $this->sageSendEmail
        ];

        if ($this->customerName) {
            $values["Customer"] = self::cleanInput($this->customerName);
        }

        if ($this->customerEmail) {
            $values["CustomerEmail"] = self::cleanInput($this->customerEmail);
        }

        if ($this->vendorEmail) {
            $values["VendorEmail"] = $this->vendorEmail;
        }

        /** Billing Information **/
        $values["BillingSurname"] = self::cleanInput($this->billingSurname);
        $values["BillingFirstnames"] = self::cleanInput($this->billingFirstNames);
        $values["BillingAddress1"] = self::cleanInput($this->billingAddress1);

        if ($this->billingAddress2) {
            $values["BillingAddress2"] = self::cleanInput($this->billingAddress2);
        }

        $values["BillingCity"] = self::cleanInput($this->billingCity);
        $values["BillingPostCode"] = self::cleanInput($this->billingPostCode, self::CLEAN_INPUT_FILTER_ALPHANUMERIC);
        $values["BillingCountry"] = $this->billingCountry;

        if ($this->billingState) {
            $values["BillingState"] = $this->billingState;
        }

        $values["BillingPostCode"] = ($values["BillingPostCode"] != "") ? $values["BillingPostCode"] : "NA";

        if ($this->billingPhone) {
            $values["BillingPhone"] = self::cleanInput($this->billingPhone);
        }

        /** Delivery Information */
        $values["DeliverySurname"] = self::CleanInput($this->deliveryIsSame ? $this->billingSurname : $this->deliverySurname);
        $values["DeliveryFirstnames"] = self::CleanInput($this->deliveryIsSame ? $this->billingFirstNames : $this->deliveryFirstNames);
        $values["DeliveryAddress1"] = self::CleanInput($this->deliveryIsSame ? $this->billingAddress1 : $this->deliveryAddress1);

        if ($this->deliveryIsSame ? $this->billingAddress2 : $this->deliveryAddress2) {
            $values["DeliveryAddress2"] = self::CleanInput($this->deliveryIsSame ? $this->billingAddress2 : $this->deliveryAddress2);
        }

        $values["DeliveryCity"] = self::CleanInput($this->deliveryIsSame ? $this->billingCity : $this->deliveryCity);
        $values["DeliveryPostCode"] = self::CleanInput($this->deliveryIsSame ? $this->billingPostCode : $this->deliveryPostCode,
            self::CLEAN_INPUT_FILTER_ALPHANUMERIC);

        $values["DeliveryCountry"] = $this->deliveryIsSame ? $this->billingCountry : $this->deliveryCountry;

        if ($this->deliveryIsSame ? $this->billingState : $this->deliveryState) {
            $values["DeliveryState"] = $this->deliveryIsSame ? $this->billingState : $this->deliveryState;
        }

        if ($this->deliveryIsSame ? $this->billingState : $this->deliveryPhone) {
            $values["DeliveryPhone"] = self::CleanInput($this->deliveryIsSame ? $this->billingPhone : $this->deliveryPhone);
        }

        $values["DeliveryPostCode"] = ($values["DeliveryPostCode"] != "") ? $values["DeliveryPostCode"] : "NA";

        $values["AllowGiftAid"] = $this->getGiftAid();
        $values["ApplyAVSCV2"] = $this->getApplyAvsCv2();
        $values["Apply3DSecure"] = $this->getApply3DSecure();

        if ($this->getPayPalAgreement()) {
            $values["BillingAgreement"] = $this->getPayPalAgreement();
        }

        /** Add the basket information to the values array */
        if (count($this->orderItems) > 0) {
            $basket = count($this->orderItems);

            foreach ($this->orderItems as $item) {
                $basket .= ":" . str_replace(":", "-", self::cleanInput($item->Description)) . ":" . $item->Quantity . ":" .
                    $item->UnitCostExVat . ":" . $item->UnitVat . ":" . $item->UnitCostIncVat . ":" . $item->TotalCostIncVat;
            }

            $values["Basket"] = $basket;
        }

        /**
         * We want to save the payment information including the customer information that we sent to sage pay.
         */
        $paymentAttempt = new PaymentAttempt();
        $paymentAttempt->PaymentID = $payment->PaymentID;
        $paymentAttempt->RequestData = serialize($values);
        $paymentAttempt->save();

        $crypt = "";

        foreach ($values as $key => $value) {
            $crypt .= "$key=$value&";
        }

        $crypt = substr($crypt, 0, -1);
        $crypt = $this->encryptAndEncode($crypt);

        return <<<HTML
		<form method="post" action="{$this->getSagePayUrl()}">
			<input type="hidden" name="VPSProtocol" value="{$this->sagePayProtocol}"/>
			<input type="hidden" name="TxType" value="{$this->getTransactionType()}"/>
			<input type="hidden" name="Vendor" value="{$this->getVendorName()}"/>
			<input type="hidden" name="Crypt" value="{$crypt}"/>
			{$this->getSubmitButtonHtml()}
		</form>
HTML;

    }

    public function getSagePayMode()
    {
        if ($this->sagePayMode == "") {
            $this->sagePayMode = self::SAGE_PAY_MODE_TEST;
        }

        return $this->sagePayMode;
    }

    public function getTransactionType()
    {
        if ($this->transactionType == "") {
            $this->transactionType = self::SAGE_PAY_TRANSACTION_TYPE_PAYMENT;
        }

        return $this->transactionType;
    }


    public function getSagePayUrl()
    {
        switch ($this->getSagePayMode()) {
            case self::SAGE_PAY_MODE_TEST:
                return self::SAGE_PAY_TEST_PURCHASE_URL;
                break;
            case self::SAGE_PAY_MODE_SIMULATOR:
                return self::SAGE_PAY_SIM_PURCHASE_URL;
                break;
            case self::SAGE_PAY_MODE_LIVE:
                return self::SAGE_PAY_LIVE_PURCHASE_URL;
                break;
            default:
                return self::SAGE_PAY_TEST_PURCHASE_URL;
                break;
        }
    }

    public function getVendorName()
    {
        if ($this->vendorName == null || $this->vendorName == "") {
            throw new PaymentProviderException("No Vendor Name Set");
        }

        return $this->vendorName;
    }


    public function getPayment()
    {
        $payment = null;

        try {
            $payment = new Payment($this->paymentID);
        } catch (RecordNotFoundException $ex) {
            $payment = new Payment();
            $payment->save();
        }

        return $payment;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        if ($this->currency == "") {
            $this->currency = self::CURRENCY_GBP;
        }

        return $this->currency;
    }

    /**
     * @return string
     */
    public function getGiftAid()
    {
        if ($this->giftAid == "") {
            $this->giftAid = self::SAGE_PAY_GIFT_AID_DISABLED;
        }

        return $this->giftAid;
    }

    public function getPayPalAgreement()
    {
        if ($this->payPalAgreement == null) {
            $this->payPalAgreement = self::SAGE_PAY_PAY_PAL_DISABLED;
        }

        return $this->payPalAgreement;
    }

    /**
     * @return string
     */
    public function getApply3DSecure()
    {
        if ($this->apply3DSecure == null) {
            $this->apply3DSecure = self::SAGE_PAY_APPLY_3D_SECURE_DEFAULT;
        }

        return $this->apply3DSecure;
    }

    /**
     * @return string
     */
    public function getApplyAvsCv2()
    {
        if ($this->applyAvsCv2 == null) {
            $this->applyAvsCv2 = self::SAGE_PAY_APPLY_AVS_CSV2_DEFAULT;
        }

        return $this->applyAvsCv2;
    }

    /**
     * @param $text
     * @param null $filter
     * @return mixed
     */
    public static function cleanInput($text, $filter = null)
    {
        if ($filter == null) {
            $filter = self::CLEAN_INPUT_FILTER_TEXT;
        }

        return preg_replace('|[^' . $filter . ']|', '', $text);
    }

    /**
     * @param $crypt
     * @return string
     * @throws PaymentProviderException
     */
    private function encryptAndEncode($crypt)
    {
        if ($this->getEncryptionType() == "XOR") {
            return self::base64Encode(self::simpleXOR($crypt, $this->getEncryptionPassword()));
        } else {
            $strIV = $this->getEncryptionPassword();

            //** add PKCS5 padding to the text to be encypted
            $crypt = self::AddPKCS5Padding($crypt);

            //** perform encryption with PHP's MCRYPT module
            $crypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->getEncryptionPassword(), $crypt, MCRYPT_MODE_CBC, $strIV);

            //** perform hex encoding and return
            return "@" . bin2hex($crypt);
        }
    }

    public function decodeAndDecrypt($crypt)
    {
        if (substr($crypt, 0, 1) == "@") {
            $strIv = $this->getEncryptionPassword();
            $crypt = substr($crypt, 1);

            $crypt = pack("H*", $crypt);

            return self::removePkcs5Padding(
                mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->getEncryptionPassword(), $crypt, MCRYPT_MODE_CBC, $strIv)
            );
        } else {
            return self::simpleXor(self::base64Decode($crypt), $this->getEncryptionPassword());
        }
    }

    /**
     * @param $decrypted
     * @return string
     */
    private static function removePkcs5Padding($decrypted)
    {
        $padChar = ord($decrypted[strlen($decrypted) - 1]);
        return substr($decrypted, 0, -$padChar);
    }

    /**
     * @param $input
     * @return string
     */
    private static function addPKCS5Padding($input)
    {
        $blockSize = 16;
        $padding = "";

        // Pad input to an even block size boundary
        $padLength = $blockSize - (strlen($input) % $blockSize);
        for ($i = 1; $i <= $padLength; $i++) {
            $padding .= chr($padLength);
        }

        return $input . $padding;
    }

    /**
     * @param $string
     * @param $key
     * @return string
     */
    public static function simpleXor($string, $key)
    {
        // Initialise key array
        $keyList = array();
        // Initialise out variable
        $output = "";

        // Convert $Key into array of ASCII values
        for ($i = 0; $i < strlen($key); $i++) {
            $keyList[$i] = ord(substr($key, $i, 1));
        }

        // Step through string a character at a time
        for ($i = 0; $i < strlen($string); $i++) {
            // Get ASCII code from string, get ASCII code from key (loop through with MOD), XOR the two, get the character from the result
            // % is MOD (modulus), ^ is XOR
            $output .= chr(ord(substr($string, $i, 1)) ^ ($keyList[$i % strlen($key)]));
        }

        // Return the result
        return $output;
    }

    /**
     * @param $plain
     * @return string
     */
    public static function base64Encode($plain)
    {
        return base64_encode($plain);
    }

    /**
     * @param $scrambled
     * @return string
     */
    private static function base64Decode($scrambled)
    {
        $scrambled = str_replace(" ", "+", $scrambled);
        return base64_decode($scrambled);
    }

    private function getEncryptionType()
    {
        if ($this->encryptionType == "") {
            $this->encryptionType = "AES";
        }

        return $this->encryptionType;
    }

    /**
     * @return string
     * @throws PaymentProviderException
     */
    public function getEncryptionPassword()
    {
        if ($this->encryptionPassword == "") {
            throw new PaymentProviderException("The Encryption Password Is Not Set ");
        }

        return $this->encryptionPassword;
    }

    /**
     * @param $data
     */
    public static function splitCryptData($data)
    {
        $out = array();
        $data = "&$data&";

        if (preg_match_all('/&([a-zA-Z0-9]+)=/', $data, $keys)) {
            for ($i = 0, $count = count($keys[0]); $i < $count; $i++) {
                if ($i == 0) {
                    $endOfValue = 0;
                } else {
                    $endOfValue = strpos($data, $keys[0][$i]);
                    $value = substr($data, 0, $endOfValue);
                    $out[$keys[1][$i - 1]] = $value;
                }

                $data = substr($data, $endOfValue + strlen($keys[0][$i]));
            }
        }
    }

    /**
     * @return string
     */
    protected function getSubmitButtonHtml()
    {
        return <<<HTML
			<input type="submit" class="btn primary large block" value="Pay Now"/>
HTML;

    }
}