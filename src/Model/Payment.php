<?php
/*
 *	Copyright 2015 RhubarbPHP
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
namespace Rhubarb\Scaffolds\Payment\Model;

use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Repositories\MySql\Schema\Columns\MySqlEnumColumn;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\DateTimeColumn;
use Rhubarb\Stem\Schema\Columns\StringColumn;
use Rhubarb\Stem\Schema\ModelSchema;


/**
 * Class Payment
 *
 * @package Rhubarb\Scaffolds\Payment\Model
 * @property int $PaymentID
 * @property \Rhubarb\Crown\DateTime\RhubarbDateTime $CreatedDate
 * @property \Rhubarb\Crown\DateTime\RhubarbDateTime $PaymentDate
 * @property string TransactionRef
 * @property \Rhubarb\Scaffolds\Payment\Model\PaymentAttempt[]|\Rhubarb\Stem\Collections\Collection $PaymentAttempts
 */
class Payment extends Model
{

    /** @var string STATUS_APPROVED */
    const STATUS_APPROVED = 'Approved';

    /** @var string STATUS_SETTLED */
    const STATUS_SETTLED = 'Settled';

    /** @var string STATUS_RAISED  */
    const STATUS_RAISED = 'Raised';

    /**
     * Returns the schema for this data object.
     *
     * @return \Rhubarb\Stem\Schema\ModelSchema
     */
    protected function createSchema()
    {
        $schema = new ModelSchema("tblPayment");
        $schema->addColumn
        (
            new AutoIncrementColumn("PaymentID"),
            new DateTimeColumn("CreatedDate"),
            new DateTimeColumn("PaymentDate"),
            new StringColumn("TransactionRef", 100),
            new MySqlEnumColumn(
                "Status",
                Payment::STATUS_RAISED,
                [
                    Payment::STATUS_RAISED,
                    Payment::STATUS_APPROVED,
                    Payment::STATUS_SETTLED
                ]
            )
        );

        $schema->uniqueIdentifierColumnName = "PaymentID";
        return $schema;
    }

    protected function beforeSave()
    {
        if ($this->isNewRecord()) {
            $this->CreatedDate = "now";
        }

        parent::beforeSave();
    }


    public function afterSave()
    {
        if ($this->TransactionRef == "") {
            $this->TransactionRef = "PGSP_" . $this->PaymentID;
        }

        parent::afterSave();
    }

    /**
     * Returns the payment status
     * @return string
     */
    public function getPaymentStatus()
    {
        $status = PaymentAttempt::STATUS_RISE;

        if ($this->PaymentAttempts->count() > 0) {
            foreach ($this->PaymentAttempts as $paymentAttempt) {
                if ($paymentAttempt->Status == PaymentAttempt::STATUS_SETTLE) {
                    $status = PaymentAttempt::STATUS_SETTLE;
                    break;
                }
            }
        }

        return $status;
    }
}