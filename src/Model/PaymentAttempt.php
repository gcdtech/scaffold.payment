<?php

/*
 *	Copyright 2015 RhubarbPHP
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace  Rhubarb\Scaffolds\Payment\Model;

use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\DateTimeColumn;
use Rhubarb\Stem\Schema\Columns\ForeignKeyColumn;
use Rhubarb\Stem\Schema\Columns\LongStringColumn;
use Rhubarb\Stem\Schema\Columns\StringColumn;
use Rhubarb\Stem\Schema\ModelSchema;

/**
 * Class PaymentAttempt
 *
 * @package Rhubarb\Scaffolds\Payment\Model
 * @property int $PaymentAttemptID
 * @property int $PaymentID
 * @property string $Gateway
 * @property string $RequestData
 * @property string $ResponseData
 * @property \Rhubarb\Crown\DateTime\RhubarbDateTime $PaymentAttemptDate
 * @property string $Status
 * @property \Rhubarb\Scaffolds\Payment\Model\Payment $Payment
 */
class PaymentAttempt extends Model
{

	/** @var $PAYMENT_GATE_WAY_REALEX */
	const PAYMENT_GATE_WAY_REALEX = 'Realex';

	/** @var $PAYMENT_GATE_WAY_SAGE_PAY */
	const PAYMENT_GATE_WAY_SAGE_PAY = 'SagePay';

	/** @var $PAYMENT_GATE_WAY_PAY_PAL */
	const PAYMENT_GATE_WAY_PAY_PAL = 'Pay Pal';

	/** @var $PAYMENT_GATE_WAY_STRIPE */
	const PAYMENT_GATE_WAY_STRIPE = 'Stripe';

	/** @var $STATUS_RISE */
	const STATUS_RISE = 'Rise';

	/** @var $STATUS_RISE */
	const STATUS_SETTLE = 'Settle';

	/**
	 * Returns the schema for this data object.
	 *
	 * @return \Rhubarb\Stem\Schema\ModelSchema
	 */
	protected function createSchema()
	{
		$schema = new ModelSchema( "tblPaymentAttempt" );
		$schema->addColumn
		(
			new AutoIncrementColumn( "PaymentAttemptID" ),
			new ForeignKeyColumn( "PaymentID" ),
			new StringColumn( "Gateway", 100 ),
			new LongStringColumn( "RequestData" ),
			new LongStringColumn( "ResponseData" ),
			new DateTimeColumn( "PaymentAttemptDate" ),
			new StringColumn( "Status", 50 )
		);

		$schema->uniqueIdentifierColumnName = "PaymentAttemptID";
		return $schema;
	}
}