<?php
/*
 *	Copyright 2015 RhubarbPHP
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
namespace Rhubarb\Scaffolds\Payment\Model;

/**
 * Class OrderItem
 * @package Rhubarb\Scaffolds\Payment\Model
 */
class OrderItem
{
    /** @var string Description */
    public $Description = "";

    /** @var int Quantity */
    public $Quantity = 1;

    /** @var float UnitCostExVat */
    public $UnitCostExVat = 0.0;

    /** @var float UnitVat */
    public $UnitVat = 0.0;

    /** @var float|null UnitCostIncVat */
    public $UnitCostIncVat = 0.0;

    /** @var float|null TotalCostIncVat */
    public $TotalCostIncVat = 0.0;

    public function __construct($description, $quantity, $unitExVat, $unitVat, $unitIncVat = null, $totalIncVat = null)
    {
        $this->Description = $description;
        $this->Quantity = $quantity;
        $this->UnitCostExVat = $unitExVat;
        $this->UnitVat = $unitVat;

        if ($unitIncVat == null) {
            $this->UnitCostIncVat = $this->UnitCostExVat + $this->UnitVat;
        } else {
            $this->UnitCostIncVat = $unitIncVat;
        }

        if ($totalIncVat == null) {
            $this->TotalCostIncVat = $this->Quantity * $this->UnitCostIncVat;
        } else {
            $this->TotalCostIncVat = $totalIncVat;
        }
    }
}