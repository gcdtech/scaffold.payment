<?php
/*
 *	Copyright 2015 RhubarbPHP
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
namespace Rhubarb\Scaffolds\Payment\Tests;

use Rhubarb\Crown\Tests\AppTestCase;
use Rhubarb\Scaffolds\Payment\Model\Payment;
use Rhubarb\Scaffolds\Payment\Model\PaymentAttempt;
use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Schema\SolutionSchema;

class PaymentModelTest extends AppTestCase
{
    private $payment;
    private $paymentAttemptA;

    public function testPaymentAdd()
    {
        $this->assertGreaterThan(0, $this->payment->PaymentID);
    }

    public function testPaymentStatus()
    {
        $status = $this->payment->PaymentStatus;

        $this->assertNotEquals(PaymentAttempt::STATUS_SETTLE, $status);
    }

    protected function setUp()
    {
        parent::setUp();

        Model::clearAllRepositories();
        SolutionSchema::getAllSchemas();

        $this->payment = new Payment();
        $this->payment->save();

        $this->paymentAttemptA = new PaymentAttempt();
        $this->paymentAttemptA->PaymentID = $this->payment->PaymentID;
        $this->paymentAttemptA->Status = PaymentAttempt::STATUS_RISE;
        $this->paymentAttemptA->save();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }
}
