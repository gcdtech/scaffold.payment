<?php
/*
 *	Copyright 2015 RhubarbPHP
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Rhubarb\Scaffolds\Payment\Tests;

use Rhubarb\Crown\Tests\AppTestCase;
use Rhubarb\Scaffolds\Payment\Model\OrderItem;
use Rhubarb\Scaffolds\Payment\Model\Payment;
use Rhubarb\Scaffolds\Payment\Providers\SagePay\SagePay;
use Rhubarb\Scaffolds\Payment\Providers\PaymentProviderException;

class SagePayTest extends AppTestCase
{
    private $sagePay;
    private $orderItem;

    public function testLoadSettings()
    {
        $this->assertEquals(1, $this->sagePay->orderID);

        $this->assertEquals(SagePay::SAGE_PAY_PROTOCOL_VERSION_3, $this->sagePay->sagePayProtocol);
        $this->assertEquals(SagePay::SAGE_PAY_SIM_PURCHASE_URL, $this->sagePay->sagePayUrl);

        $this->assertNotNull($this->sagePay->amount);
    }

    public function testCurrency()
    {
        $this->assertNotNull($this->sagePay->currency);
        $this->assertEquals(SagePay::CURRENCY_GBP, $this->sagePay->currency);
        $this->assertNotEquals(SagePay::CURRENCY_EURO, $this->sagePay->currency);
    }

    public function testSagePayMode()
    {
        $this->assertNotNull(SagePay::SAGE_PAY_MODE_TEST, $this->sagePay->getSagePayMode());
    }

    public function testPaymentRefIsSet()
    {
        $payment = new Payment();
        $payment->save();

        $this->assertNotNull($payment->TransactionRef);
    }

    public function testSagePayTransactionType()
    {
        $this->assertNotNull($this->sagePay->getTransactionType());
        $this->assertEquals(SagePay::SAGE_PAY_TRANSACTION_TYPE_PAYMENT, $this->sagePay->transactionType);
    }

    public function testVendorName()
    {
        $this->assertNotNull($this->sagePay->getVendorName());
    }

    public function testEncrptionPasswordException()
    {
        try {
            $this->sagePay->getHtml();
        } catch (PaymentProviderException $ex) {
            echo $ex->getMessage();
        }
    }

    public function testSagePageToString()
    {
        try {
            $returnValue = $this->sagePay->__toString();
            $this->assertContains("TxType", $returnValue);
        } catch (PaymentProviderException $ex) {
            $this->fail($ex->getMessage());
        }
    }

    public function testAddOrderItem()
    {
        $this->sagePay->orderItems[] = $this->orderItem;
        $this->assertNotEmpty($this->sagePay->orderItems);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->orderItem = new OrderItem("Test Order Item", 1, 20, 0);

        $this->sagePay = new SagePay();
        $this->sagePay->vendorName = "Gcd Tech";
        $this->sagePay->encryptionPassword = $this->randomPasswordGenerate();

        $this->sagePay->sagePayProtocol = SagePay::SAGE_PAY_PROTOCOL_VERSION_3;
        $this->sagePay->sagePayUrl = SagePay::SAGE_PAY_SIM_PURCHASE_URL;
        $this->sagePay->currency = SagePay::CURRENCY_GBP;

        $this->sagePay->orderID = 1;
        $this->sagePay->amount = 100;

        $this->sagePay->orderItems[] = $this->orderItem;
    }

    /**
     * This function is used to generate an encryption password
     * for testing the mcrypt function
     *
     * @param int $length
     *
     * @return string
     */
    private function randomPasswordGenerate($length = 16)
    {
        $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $str = '';
        $count = strlen($charset);
        while ($length--) {
            $str .= $charset[mt_rand(0, $count - 1)];
        }

        return $str;
    }
}
